#!/usr/bin/env python

import sys, os
import unittest
import subprocess
import shutil, tempfile

from zeroinstall import support
from zeroinstall.zerostore import unpack

mydir = os.path.dirname(__file__)
pkg2zero = os.path.join(mydir, '..', 'pkg2zero')

def run(args, **kwargs):
	child = subprocess.Popen([pkg2zero] + args, stdout = subprocess.PIPE, stderr = subprocess.PIPE, stdin = subprocess.PIPE, **kwargs)
	return child.communicate("\n\n")

TEST_DATA = os.environ["TEST_DATA"]
def testdata(path):
	return os.path.join(TEST_DATA, path)

class TestAll(unittest.TestCase):
	def setUp(self):
		self.tmpdir = tempfile.mkdtemp('pkg2zero')
		os.environ['HOME'] = self.tmpdir
		os.environ['PATH'] = mydir + ':' + os.environ['PATH']
	
	def tearDown(self):
		support.ro_rmtree(self.tmpdir)

	def testHelp(self):
		cout, cerr = run(["--help"])
		assert 'show this help message and exit' in cout, cout
		assert not cerr, cerr
	
	def testDeb(self):
		target = os.path.join(self.tmpdir, 'netcat.xml')
		cout, cerr = run(["http://ftp.uk.debian.org/debian/pool/main/n/netcat/netcat-traditional_1.10-38_i386.deb", target])
		assert 'Added version 1.10-38 to ' in cout, (cout, cerr)
		assert 'Mappings file not found' in cerr
		assert 'no SHA-1' in cerr

		cout, cerr = run(["http://ftp.uk.debian.org/debian/pool/main/n/netcat/netcat-traditional_1.10-38_i386.deb", target])
		assert 'already contains an implementation with this digest' in cerr, cerr

		child = subprocess.Popen(['0launch', target, '-h'], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
		cout, cerr = child.communicate()
		assert 'connect to somewhere' in cerr, cerr
		assert not cout, cout

	def testDebTarball(self):
		deb_file = "netcat-traditional_1.10-38_i386.deb"
		build_dir = os.path.join(self.tmpdir, 'build')
		os.mkdir(build_dir)
		unpack.unpack_archive(deb_file, open(deb_file), destdir = build_dir)
		tar_file = "netcat-traditional.tar.bz2"
		subprocess.check_call(['tar', 'cjf', tar_file, 'build'], cwd = self.tmpdir)

		target = os.path.join(self.tmpdir, 'netcat.xml')
		cout, cerr = run(['--archive-extract=build', '--archive-url', "http://localhost/archives/" + tar_file, os.path.abspath(deb_file), target], cwd = self.tmpdir)
		assert 'Added version 1.10-38 to ' in cout, (cout, cerr)
		assert 'Mappings file not found' in cerr
		assert 'no SHA-1' in cerr

		child = subprocess.Popen(['0launch', target, '-h'], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
		cout, cerr = child.communicate()
		assert 'connect to somewhere' in cerr, cerr
		assert not cout, cout

	def testRPM(self):
		target = os.path.join(self.tmpdir, 'time.xml')
		cout, cerr = run(['-r', testdata('repodata/repomd.xml'), "http://mirror.centos.org/centos/5/os/i386/CentOS/time-1.7-27.2.2.i386.rpm", target])
		assert 'Added version 1.7 to ' in cout, (cout, cerr)
		assert 'Mappings file not found' in cerr
		assert 'no SHA-1' in cerr

		child = subprocess.Popen(['0launch', target, 'echo', 'hi'], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
		cout, cerr = child.communicate()
		assert 'hi' in cout, cout
		assert 'pagefaults' in cerr, cerr

	def testRepoDeb(self):
		target = os.path.join(self.tmpdir, 'netcat.xml')
		cout, cerr = run(["netcat-traditional", '--packages-file', testdata('Packages.bz2'), target])
		assert 'Added version 1.10-38 to ' in cout, (cout, cerr)
		assert 'Mappings file not found' in cerr
		assert "Package's digest matches value in reposistory metadata" in cout, (cout, cerr)

		child = subprocess.Popen(['0launch', target, '-h'], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
		cout, cerr = child.communicate()
		assert 'connect to somewhere' in cerr, cerr
		assert not cout, cout


	def testRepoRPM(self):
		target = os.path.join(self.tmpdir, 'time.xml')
		cout, cerr = run(["time", '--repomd-file', testdata('repodata/repomd.xml'), target])
		assert 'Added version 1.7 to ' in cout, (cout, cerr)
		assert 'Mappings file not found' in cerr
		assert "Package's digest matches value in reposistory metadata" in cout, (cout, cerr)

		child = subprocess.Popen(['0launch', target, 'echo', 'hi'], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
		cout, cerr = child.communicate()
		assert 'hi' in cout, cout
		assert 'pagefaults' in cerr, cerr

if __name__ == '__main__':
	sys.argv.append('-v')
	unittest.main()
