# Copyright (C) 2008, Thomas Leonard
# See the COPYING file for details, or visit http://0install.net.

import sys, time
from optparse import OptionParser
import tempfile, shutil, os
from xml.dom import minidom
import subprocess

from zeroinstall.injector import model, qdom, distro
from zeroinstall.injector.namespaces import XMLNS_IFACE
from zeroinstall.support import basedir

config_site = '0install.net'
config_prog = 'pkg2zero'

def read_child(cmd):
	child = subprocess.Popen(cmd, stdout = subprocess.PIPE)
	output, unused = child.communicate()
	if child.returncode:
		print >>sys.stderr, output
		print >>sys.stderr, "%s: code = %d" % (' '.join(cmd), child.returncode)
		sys.exit(1)
	return output

def add_node(parent, element, text = None, before = '  ', after = '\n'):
	doc = parent.ownerDocument
	parent.appendChild(doc.createTextNode(before))
	new = doc.createElementNS(XMLNS_IFACE, element)
	parent.appendChild(new)
	if text:
		new.appendChild(doc.createTextNode(text))
	parent.appendChild(doc.createTextNode(after))
	return new

class Mappings:
	def __init__(self):
		self.last_base = 'http://0install.net/2008/3rd-party'	# For suggesting new URIs
		self.mappings = {}
		mappings_file = basedir.load_first_config(config_site, config_prog, 'mappings')
		if mappings_file:
			self.load(mappings_file)
		else:
			print >>sys.stderr, "Mappings file not found; dependencies will not be included."

	def load(self, mappings_file):
		for line in file(mappings_file):
			if ':' in line:
				pkg, zero = [x.strip() for x in line.split(':', 1)]
				if pkg in self.mappings:
					print "Mapping for %s given twice in %s!" % (pkg, mappings_file)
				self.mappings[pkg] = zero
				self.last_base = zero.rsplit('/', 1)[0]

	def process(self, s):
		parts = [x.strip() for x in s.split('(', 1)]
		if len(parts) == 2:
			pkg, restrictions = parts
		else:
			pkg = parts[0]
			restrictions = None

		if pkg not in self.mappings:
			print "Ignoring dependency on %s; not in mappings file" % pkg
			return None

		e = qdom.Element(XMLNS_IFACE, "requires", {})
		r = model.InterfaceDependency(self.mappings[pkg], element = e)
		# TODO: version restrictions
		return r

	def lookup(self, pkg):
		return self.mappings.get(pkg, None)

	def add_mapping(self, pkg, uri):
		self.mappings[pkg] = uri
		config_dir = basedir.save_config_path(config_site, config_prog)
		mappings_file = os.path.join(config_dir, 'mappings')
		stream = file(mappings_file, 'a')
		stream.write("%s: %s\n" % (pkg, uri))
		stream.close()
		print "Wrote name mapping to %s" % mappings_file

	def get_suggestion(self, pkg_name):
		return "%s/%s.xml" % (self.last_base, pkg_name)
